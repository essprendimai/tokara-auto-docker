#!/usr/bin/env bash

source ./parameters.sh

# Setup base path ------------------------------------------------------------------------------------------------------

if [ ! -d ${PROJECTS_ROOT} ]; then
    mkdir -vp ${PROJECTS_ROOT};
    chmod 777 ${PROJECTS_ROOT};
fi

# Checkout projects ----------------------------------------------------------------------------------------------------

if [ ! -d ${LARAVEL_DIRECTORY} ]; then
  git clone ${LARAVEL_GIT} ${LARAVEL_DIRECTORY};
fi

if [ ! -d ${FLUENTD_DIRECTORY} ]; then
  git clone ${FLUENTD_GIT} ${FLUENTD_DIRECTORY};
fi

if [ ! -d ${SUPERVISOR_DIRECTORY} ]; then
  git clone ${SUPERVISOR_GIT} ${SUPERVISOR_DIRECTORY};
fi
