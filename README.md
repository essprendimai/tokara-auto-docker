# Setup git if not have account

* git config --global user.name "GitName"
* git config --global user.email "GitEmail"
* ssh-keygen -t rsa -C "GitEmail"

# Dockerized project

You can view complete list of containers at [docker-compose.yml](docker-compose.yml).

#Setup:

1. Install [docker](https://docs.docker.com/engine/installation/) 
and [docker-compose](https://docs.docker.com/compose/). 
Complete [post-install#manage-docker-as-a-non-root-user](https://docs.docker.com/engine/installation/linux/linux-postinstall/#/manage-docker-as-a-non-root-user)
instructions.
2. Verify that you have correct docker and docker-compose versions.
```docker -v``` should output ```Docker version 1.13.0, build 49bf474```.
and ```docker-compose -v``` should output ```docker-compose version 1.10.0, build 4bd6f1a```
3. Generate new password-less SSH key at ```~/.ssh/id_rsa```.
4. Add ```~/.ssh/id_rsa.pub``` public key to your Bitbucket account.
5. Run ```./init.sh```. It will take care of everything. It runs (```./checkout.sh```, ```./build.sh``` and ```./setup.sh```) 
> **Note**: It may fail on the first try, because mariadb and redis takes time to initialize. In case of database connection error just run ```setup.sh``` again.
6. And run ```./start.sh``` to start development server.

#Setup fluentd

1. Change mail settings at docker-compose.yml

## FAQ

1. How to build a **single** dockerized service from docker-compose.yml?
```docker-compose build {service-name}```

2. How to start a **single** dockerized service from docker-compose.yml?
```docker-compose up {service-name}```

3. How do I run my custom command on container?
```docker-compose run {service-name} {custom-command}```

3. Something is broken and I want to start over, what command I should run?
```./docker-nuke.sh```

**DANGER:** It will wipe **ALL** system containers, images and volumes.

4. How do I access docker containers from host OS?
* Project: [http://localhost:8000](http://localhost:8000)
* mariadb: ```localhost:3306```, root password: ```secret```
* fluentd: ```localhost:8888```

5. How do I keep my repositories up to date?
Run ```./update-repositories.sh``` bash script. It will update all repositories if they
are on ```master``` branch.

6. How do I run xdebug on ```tokara-auto-laravel```?
Set session name cookie to PHPSTORM. Set remote server absolute path to ```/home/docker/src```.

7. How to setup PHP Docker Remote interpreter?
[Run PHPUnit tests inside a docker container from PhpStorm](https://blog.alejandrocelaya.com/2017/02/01/run-phpunit-tests-inside-docker-container-from-phpstorm/)

8. When to use --service-ports
If running docker-compose run not setting host 0.0.0.0 to ports. (https://github.com/docker/compose/issues/1259)