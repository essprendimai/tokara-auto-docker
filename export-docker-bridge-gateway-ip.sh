#!/usr/bin/env bash

export DOCKER_BRIDGE_GATEWAY_IP=`docker network inspect -f "{{range .IPAM.Config}}{{.Gateway}}{{end}}" bridge`