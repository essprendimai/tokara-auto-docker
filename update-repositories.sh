#!/bin/bash

set -euo pipefail

update_repository () {
    local repo_name=$1
    echo $repo_name "repository"
    local path="projects/$repo_name"
    cd $path
    local current_branch=`git rev-parse --abbrev-ref HEAD`
    if [ "$current_branch" == "master" ]
    then
        git pull origin develop
    else
        echo "Not on master branch"
    fi
    echo " "
    cd -
}

update_repository "tokara-auto-laravel"
update_repository "tokara-auto-fluentd"
update_repository "tokara-supervisor"
