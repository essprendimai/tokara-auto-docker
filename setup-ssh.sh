#!/usr/bin/env bash

cp -f ~/.ssh/id_rsa projects/tokara-auto-laravel/docker/id_rsa
cp -f ~/.ssh/id_rsa.pub projects/tokara-auto-laravel/docker/id_rsa.pub

