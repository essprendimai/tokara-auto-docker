#!/usr/bin/env bash

# ----------------------------------------------------------------------------------------------------------------------

# Base paths
PROJECTS_ROOT=projects;

# ----------------------------------------------------------------------------------------------------------------------

# Project paths
LARAVEL_DIRECTORY=${PROJECTS_ROOT}/tokara-auto-laravel;
FLUENTD_DIRECTORY=${PROJECTS_ROOT}/tokara-auto-fluentd;
SUPERVISOR_DIRECTORY=${PROJECTS_ROOT}/tokara-supervisor;

# Project repositories
LARAVEL_GIT=git@bitbucket.org:essprendimai/tokara-auto-laravel.git;
FLUENTD_GIT=git@bitbucket.org:essprendimai/tokara-auto-fluentd.git;
SUPERVISOR_GIT=git@bitbucket.org:essprendimai/tokara-supervisor.git

# Project containers
LARAVEL_CONTAINER=tokara-auto-laravel

# ----------------------------------------------------------------------------------------------------------------------
