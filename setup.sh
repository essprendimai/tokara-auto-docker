#!/usr/bin/env bash

# toakra auto laravel
cp projects/tokara-auto-laravel/.env.example projects/tokara-auto-laravel/.env
docker-compose run --rm tokara-auto-laravel bash -c "cd src/tokara-auto-laravel && composer install --no-interaction"
docker-compose run --rm tokara-auto-laravel bash -c "cd src/tokara-auto-laravel && php artisan key:generate"
docker-compose run --rm tokara-auto-laravel bash -c "cd src/tokara-auto-laravel && php artisan migrate:fresh"
docker-compose run --rm tokara-auto-laravel bash -c "cd src/tokara-auto-laravel && php artisan module:migrate"
docker-compose run --rm tokara-auto-laravel bash -c "cd src/tokara-auto-laravel && php artisan storage:link"
docker-compose run --rm tokara-auto-laravel bash -c "cd src/tokara-auto-laravel && php artisan db:seed"
docker-compose run --rm tokara-auto-laravel bash -c "cd src/tokara-auto-laravel && php artisan module:seed"
docker-compose run --rm tokara-auto-laravel bash -c "cd src/tokara-auto-laravel && php artisan ip-restriction:load-ips"
docker-compose run --rm tokara-auto-laravel bash -c "cd src/tokara-auto-laravel && php artisan passport:install"
